<?php

namespace app\controllers;

use app\models\EntityImage;
use Yii;
use app\models\Post;
use app\models\PostSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?']
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Post model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $model->shows++;
        $model->save();

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionTest(){
    }


    public function actionUpdate()
    {
        $id = Yii::$app->request->get('id');

        $model =  $id? $this->findModel($id) : new Post();
        $model->load(Yii::$app->request->post());
        if(Yii::$app->request->post()){
            $model->load(Yii::$app->request->post());
            $model->user_id = Yii::$app->user->id;
            $model->save();
            if(isset($_FILES )) {
                $entity = new EntityImage();
                $entity->entity = $model::className();
                $entity->entity_id = strval($model->id);

                $files = UploadedFile::getInstances($model, 'files');
                foreach ($files as $file) {
                    $ext = $file->extension;
                    $entity->path = Yii::$app->security->generateRandomString().".{$ext}";
                    if($entity->save()) {
                        $file->saveAs('uploads/' . $entity->path);
                    } else {
                        echo "<pre>"; var_dump($entity->getErrors()); die();
                    }
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
