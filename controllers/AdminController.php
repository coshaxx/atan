<?php
/**
 * Created by PhpStorm.
 * User: Dima
 * Date: 27.05.2017
 * Time: 19:55
 */

namespace app\controllers;


use Yii;
use yii\base\Controller;
use yii\filters\AccessControl;
use yii\widgets\Menu;

class AdminController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => false,
                        'roles' => ['?']
                    ]
                ],
            ],

        ];
    }

    public function actionIndex(){
      return $this->render('menu');
    }
}