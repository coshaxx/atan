<?php

namespace app\controllers;

use app\components\AuthHandler;
use app\models\EntityImage;
use app\models\Post;
use app\models\PostSearch;
use app\models\Profile;
use app\models\SignUpForm;
use app\models\User;
use Yii;
use yii\authclient\AuthAction;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\UploadedFile;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => AuthAction::class,
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
        ];
    }

    public function onAuthSuccess($client)
    {

        (new AuthHandler($client))->handle();
    }


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }


    public function actionSignUp(){
        $form = new SignUpForm();

        if(!Yii::$app->user->isGuest){
            Yii::$app->user->logout();
        }

        if (Yii::$app->request->post('SignUpForm')) {
            $post = Yii::$app->request->post('SignUpForm');
            if($form->load(Yii::$app->request->post()) &&$form->validate()) {
                /**@var $user User*/
                $user = new User();
                $user->scenario = 'create';
                $user->email = $post['email'];
                $user->setPassword($post['password']);
                $user->name = $post['name'];

                if ($user->save()) {
                    $profile = new Profile();
                    $profile->user_id = $user->id;
                    $profile->city = $post['city'];
                    $profile->category = $post['category'];
                    $profile->text = $post['text'];
                    $profile->save();
                    Yii::$app->user->login($user, $post['rememberMe'] ? 3600 * 24 * 30 : 0);
                    return $this->goBack();
                } else {
                    echo "<pre>";
                    var_dump($user->getErrors());
                    die();
                }
            }

        }

        return $this->render('signUp', [
            'model' => $form,
        ]);
    }

    public function actionFileUpload(){
        $entity = new EntityImage();
        if(isset($_FILES )&& !empty($_POST['entity'])) {
            $entity->entity = $_POST['entity'];
            $files = UploadedFile::getInstancesByName('files');
            foreach ($files as $file) {
                $name = $file->name;
               $ext = end(explode(".", $name));
                $entity->path = Yii::$app->security->generateRandomString().".{$ext}";
                if($entity->save()) {
                    $file->saveAs('uploads/' . $entity->path);
                } else{
                    echo "<pre>"; var_dump($entity->getErrors()); die();
                }
            }
        }
    }

    public function actionTest(){
        $post = Post::findOne(2);
        echo "<pre>"; var_dump($post->getImages()); die();
    }
    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionPassword(){
        /* @var User $user */
        $user = Yii::$app->user->identity;
        $user->setPassword('123');
        $user->save();
        echo'saved';
    }
}
