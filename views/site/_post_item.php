<?php
/**
 * @var $model Post
 */
use app\models\Post;
use yii\bootstrap\Html;
?>
<div class="post">
    <div class="white_block">
        <a href="<?= \yii\helpers\Url::to(['post/view','id' => $model->id])?>">
            <img src="<?= $model->getImages()[0] ?>" height="250" width="200" class="cover">
        </a>
        <div class="white_block post_icons_block" >
            <span class="icon">
                <i class="fa fa-eye" aria-hidden="true"></i><?= $model->shows ?>
            </span>
            <span class="icon">
            <i class="fa fa-heart" aria-hidden="true"></i><?= rand(13,1000) ?>
                </span>
            <span class="icon">
                <i class="fa fa-comment" aria-hidden="true"></i><?= rand(13,1000) ?>
            </span>
        </div>
    </div>
    <div class="user">
        <table>
            <tr>
                <td>
                    <?= Html::img($model->author->getPhoto(),['class' => 'user_icon']) ?>
                </td>
                <td>
                    <?= $model->author->name ?>
                </td>
            </tr>
        </table>
    </div>
</div>
