<?php

/* @var $this yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

use yii\widgets\ListView;

$this->title = 'My Yii Application';
?>
<div class="site-index">
        <div class="row">
            <?=
                ListView::widget([
                    'dataProvider' => $dataProvider,
                    'options' => [
                        'tag' => 'div',
                        'class' => 'list-wrapper',
                        'id' => 'list-wrapper',

                    ],
                    'itemOptions' => [
                        'class' => 'post_container'
                    ],
                    'itemView' => function ($model, $key, $index, $widget) {
                        return $this->render('_post_item', ['model' => $model]);
                    },
                    'layout' => "{items}\n{summary}\n{pager}\n",
                ]);
            ?>
        </div>

</div>
