<?php

use app\models\Post;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;


/* @var $this yii\web\View */
/* @var $model app\models\Post */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post-form">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
    <?php

    foreach ($model->getImages() as $image) {
        echo Html::img(Url::to('/uploads/'.$image),['class' => 'prev_img']);
    }?>
    </div>
    <div class="row">
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?php echo FileInput::widget([
        'model' => $model,
        'attribute' => 'files[]',
        'options'=>[
            'multiple'=>true,
            'resizeImages' => true,

        ],
        'resizeImages' => true,
        'pluginOptions' => [
            'resizeImages' => true
//            'uploadUrl' => Url::to(['/site/file-upload']),
//            'uploadExtraData' => [
//                'entity' => Post::className(),
//            ],
//            'maxFileCount' => 10
        ]
    ]); ?>
    </div>
    <div class="row">
        <div class="form-group" style="margin-top: 15px">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
