<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Design Hunter',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo "<form class='navbar-form navbar-left' role='search' action='/site/index'>
      <div class=\"inner-addon left-addon\">
      <i class=\"glyphicon glyphicon-search\"></i>
      <input id='searchbox' name='q' type=\"text\" class=\"form-control\" placeholder=\"Hunt...\" />
    </div>
  </form>";
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left','style' => 'margin-left: 5%;'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'Post', 'url' => ['/post/index']],
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            Yii::$app->user->isGuest ?
                (['label' => 'Sign Up', 'url' => ['/site/sign-up']])
                :
                (['label' => 'Profile', 'url' => ['/user/update','id' => Yii::$app->user->id]]),
            Yii::$app->user->isGuest ? (
                ['label' => 'Sign In', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->name . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);

    NavBar::end();
    ?>

    <div class="container">
        <div class="row">
            <div class="main_title text-center">

                Hunt for creativity!
            </div>
        </div>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
