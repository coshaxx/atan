<?php

/* @var $this yii\web\View */

use yii\widgets\Menu;

$this->title = 'Admin menu';
?>
<div class="site-index">

    <?php echo Menu::widget([
        'items' => [
            // Important: you need to specify url as 'controller/action',
            // not just as 'controller' even if default action is used.
            ['label' => 'Home', 'url' => ['site/index']],
            // 'Products' menu item will be selected as long as the route is 'product/index'
            ['label' => 'Post', 'url' => ['post/index'], 'items' => [
                ['label' => 'New Post', 'url' => ['post/update']],
            ]],
            ['label' => 'Users', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
        ],
    ]); ?>
</div>
