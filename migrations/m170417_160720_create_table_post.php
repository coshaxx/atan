<?php

use yii\db\Migration;

class m170417_160720_create_table_post extends Migration
{
    public function up()
    {
        $this->createTable('post',[
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull()
        ]);
    }

    public function down()
    {
        $this->dropTable('post');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
