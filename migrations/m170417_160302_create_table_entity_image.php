<?php

use yii\db\Migration;

class m170417_160302_create_table_entity_image extends Migration
{
    public function up()
    {
        $this->createTable('entity_image',[
            'id' => $this->primaryKey()->notNull(),
            'entity' => $this->string(255)->notNull(),
            'entity_id' => $this->string(255)->notNull(),
            'path' => $this->string(255)->notNull()
        ]);

    }

    public function down()
    {
        $this->dropTable('entity_image');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
