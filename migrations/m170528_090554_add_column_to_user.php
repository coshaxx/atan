<?php

use yii\db\Migration;

class m170528_090554_add_column_to_user extends Migration
{
    public function up()
    {
        $this->createTable('profile',[
            'user_id' => $this->primaryKey(),
            'category' => $this->string()->notNull(),
            'city' => $this->string()->notNull(),
            'text' => $this->text()->notNull(),
        ]);

        $this->addForeignKey('FK_profile__user_id','profile','user_id','user','id','cascade','cascade');

    }

    public function down()
    {
       $this->dropForeignKey('FK_profile__user_id','profile');
       $this->dropTable('profile');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
