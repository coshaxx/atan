<?php

use yii\db\Migration;

class m170528_122038_add_column_to_post extends Migration
{
    public function up()
    {
        $this->addColumn('post','shows','int not null default 1');

    }

    public function down()
    {
        $this->dropColumn('post', 'shows');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
