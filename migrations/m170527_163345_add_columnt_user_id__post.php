<?php

use yii\db\Migration;

class m170527_163345_add_columnt_user_id__post extends Migration
{
    public function up()
    {
        $this->addColumn('post','user_id','int(11) not null');
        $this->addForeignKey('FK__user_id','post','user_id','user','id','cascade','cascade');
    }

    public function down()
    {
       $this->dropForeignKey('FK__user_id', 'post');
       $this->dropColumn('post','user_id');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
