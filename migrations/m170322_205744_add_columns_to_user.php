<?php

use yii\db\Migration;

class m170322_205744_add_columns_to_user extends Migration
{
    public function up()
    {
        $this->addColumn('user','photo','varchar(255)');
        $this->addColumn('user', 'name', 'varchar(255)');
    }

    public function down()
    {
        $this->dropColumn('user', 'photo');
        $this->dropColumn('user', 'name');
    }
}
