<?php
/**
 * Created by PhpStorm.
 * User: Dima
 * Date: 28.05.2017
 * Time: 14:14
 */

namespace app\assets;


use yii\web\AssetBundle;

class FontAwesomeAsset extends AssetBundle
{
    public $sourcePath = '@vendor/components/font-awesome';
    public $css = [
        'css/font-awesome.css'
    ];
}