<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entity_image".
 *
 * @property integer $id
 * @property string $entity
 * @property string $entity_id
 * @property string $path
 */
class EntityImage extends \yii\db\ActiveRecord
{

    public $files;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'entity_image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entity', 'entity_id', 'path'], 'required'],
            [['entity', 'entity_id', 'path'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'entity' => 'Entity',
            'entity_id' => 'Entity ID',
            'path' => 'Path',
        ];
    }
}
