<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class SignUpForm extends Model
{
    public $email;
    public $password;
    public $name;
    public $rememberMe = true;

    public $city;
    public $category;
    public $text;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email', 'password','name'], 'required'],
            [['city','category','text'],'safe'],
            ['email', 'checkAlreadyUse'],

        ];
    }

    public function checkAlreadyUse($attribute, $params){
        $user = User::findByEmail($this->{$attribute});
        if($user) {
            $this->addError($attribute,'this email already use');
        }
    }


}
