<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post".
 *
 * @var array $images
 * @property integer $id
 * @property integer $user_id
 * @property integer $shows
 * @property string $title
 *
 * @property User $author
 */
class Post extends \yii\db\ActiveRecord
{
    const NO_IMAGE_FILE = 'no-image.png';
    public  $files;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title','user_id'], 'required'],
            [['title'], 'string', 'max' => 255],
            [['user_id','shows'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'user_id' => 'User ID',
            'shows' => 'Count View'
        ];
    }

    /**
     * @return array
     */
    public function getImages(){
       $images = array_map(function($entityt){
           return '/uploads/'.$entityt->path;},EntityImage::find()
           ->select('path')
           ->where([
               'entity' => Post::className(),
               'entity_id' => $this->id,
           ])->all());
       return !empty($images)? $images : [self::NO_IMAGE_FILE];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor(){
        return $this->hasOne(User::className(),['id' => 'user_id']);
    }

    /**
     * @return string
     */
    public function getImage(){
        return $this->getImages()[0];
    }
}
